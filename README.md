# Stargaze (Bin Repo)

## Index

- [Description](#Description)
- [Project Structure](#Project-Structure)

## Description

- This repo contains only the heavy binary files that are meant to be exported to another format to be used to compile the game, like Blend or Substance files

## Project Structure

```
|- Assets
    |- Cool asset
        |- CoolAsset.blend
        |- CoolAsset.spp
    |- ...
```